# DIAL Open Source Center

## Program Documentation

This repository serves as the documentation for the DIAL Open Source Center.  It is build as a [readthedocs](https://readthedocs.org/) site that is viewable at [http://osc.dial.community/en/latest/](http://osc.dial.community/en/latest/).
