.. _code-of-conduct:

Community-Wide Code of Conduct
==============================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/community-wide-code-of-conduct/927

The entire program documentation can be found at: https://hub.osc.dial.community/docs
