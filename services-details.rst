.. _services-details:

***********************
Available Services Menu
***********************

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/services/service-details/918

The entire program documentation can be found at: https://hub.osc.dial.community/docs
