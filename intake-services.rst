.. _intake-services:

###########################
OSC Project Self-Assessment
###########################

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/resources/project-self-asssessment/924

The entire program documentation can be found at: https://hub.osc.dial.community/docs
