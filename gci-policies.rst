.. _gci-policies:

========================================
Google Code-in @ DIAL Open Source Center
========================================

OSC Program documentation has moved. The new documentation can be found at the OSC Hub: https://hub.osc.dial.community/docs
