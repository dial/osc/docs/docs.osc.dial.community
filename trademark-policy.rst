.. _trademark-policy:

Community & Project Trademark Policy
====================================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/community-project-trademark-policy/940

The entire program documentation can be found at: https://hub.osc.dial.community/docs
