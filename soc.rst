.. _soc:

========================================
Summer of Code @ DIAL Open Source Center
========================================

This document has moved. The new document can be found at: https://hub.osc.dial.community/gsoc

The entire program documentation can be found at: https://hub.osc.dial.community/docs

