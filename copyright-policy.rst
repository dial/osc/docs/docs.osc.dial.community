.. _copyright-policy:

Copyright & DMCA Policy
=======================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/copyright-dmca-policy/938

The entire program documentation can be found at: https://hub.osc.dial.community/docs
