.. _governance-policy:

OSC Governance Policy
=====================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/osc-governance-policy/928

The entire program documentation can be found at: https://hub.osc.dial.community/docs
