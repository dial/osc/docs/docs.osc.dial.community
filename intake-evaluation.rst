.. _intake-evaluation:

###############################
OSC Project Maturity Evaluation
###############################

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/resources/project-maturity-evaluation/926

The entire program documentation can be found at: https://hub.osc.dial.community/docs
