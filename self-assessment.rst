.. _self-assessment:

===============================
OSC Project Membership Proposal
===============================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/resources/membership-proposal/925

The entire program documentation can be found at: https://hub.osc.dial.community/docs
