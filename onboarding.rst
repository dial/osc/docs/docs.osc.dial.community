.. _onboarding:

####################################
Steps for Getting Started in the OSC
####################################

OSC Program documentation has moved. The new documentation can be found at the OSC Hub: https://hub.osc.dial.community/docs
