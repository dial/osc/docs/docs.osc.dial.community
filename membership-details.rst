.. _membership-details:

#################
Why Join the OSC?
#################

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/about/why-join-the-osc/898

The entire program documentation can be found at: https://hub.osc.dial.community/docs
