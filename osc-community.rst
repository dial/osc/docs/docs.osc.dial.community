.. _osc-community:

###########
How We Work
###########

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/about/how-the-osc-works/899

The entire program documentation can be found at: https://hub.osc.dial.community/docs
