.. _contribution-policy:

Contribution Policy
===================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/contribution-policy/937

The entire program documentation can be found at: https://hub.osc.dial.community/docs
