.. _grants:

#####################
Catalytic Grantmaking
#####################

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/services/catalytic-grantmaking/919

The entire program documentation can be found at: https://hub.osc.dial.community/docs

