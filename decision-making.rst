.. _decision-making:

Decision Making Process: Recommendations
========================================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/governance/decision-making-process-recommendations/936

The entire program documentation can be found at: https://hub.osc.dial.community/docs
