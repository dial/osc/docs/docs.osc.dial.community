.. _soc-template-student:

===============================
Summer of Code Student Template
===============================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/services/gsoc-student-template/922

The entire program documentation can be found at: https://hub.osc.dial.community/docs
