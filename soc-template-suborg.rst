.. _soc-template-suborg:

===============================
Summer of Code Sub-Org Template
===============================

This document has moved. The new document can be found at: https://hub.osc.dial.community/k/services/gsoc-sub-org-template/921

The entire program documentation can be found at: https://hub.osc.dial.community/docs
